package main

import (
	"fmt"

	"github.com/eiannone/keyboard"
)

// rune is a single character that used to build a string
var keyPressChan chan rune

func main() {
	keyPressChan = make(chan rune)
	go listenForKeyPress()

	fmt.Println("Press any key, or q to quit")
	_ = keyboard.Open()

	defer func() {
		keyboard.Close()
	}()

	for {
		char, _, _ := keyboard.GetSingleKey()
		if char == 'q' || char == 'Q' {
			break
		} else {
			keyPressChan <- char
		}
	}
}

func listenForKeyPress() {
	for {
		key := <-keyPressChan
		fmt.Println("You Pressed", string(key))
	}
}
