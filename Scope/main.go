package main

import (
	"fmt"
	"myApp/packageone"
)

// package level variable
var one = "One"

func main() {
	// block level variable
	var one = "this is block level variable"
	fmt.Println(one)
	myFunc()
	newString := packageone.PublicVar
	fmt.Println(newString)
}

func myFunc() {
	fmt.Println(one)
}
