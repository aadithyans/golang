package main

import "fmt"

type Animal struct {
	Name         string
	Sound        string
	NumberOfLegs int
}

func (a *Animal) Says() {
	fmt.Printf("A %s says %s ", a.Name, a.Sound)
	fmt.Println()
}

func main() {
	// maps are keys and values
	// maps are not sorted, fast
	intMap := make(map[string]int)
	intMap["one"] = 1
	intMap["two"] = 2
	intMap["three"] = 3
	intMap["four"] = 4
	intMap["five"] = 5

	for key, value := range intMap {
		fmt.Println(key, value)
	}

	delete(intMap, "four")
	fmt.Println("----------")
	fmt.Println("After Delete")
	fmt.Println("--------------")
	for key, value := range intMap {
		fmt.Println(key, value)
	}
	el, ok := intMap["four"]
	if ok {
		fmt.Println(el, "is in map")
	} else {
		fmt.Println(el, "not is in map")
	}

	intMap["two"] = 4
	fmt.Println(intMap)
	myTotal := sumMany(1, 2, 3, 4, 5, 6)
	fmt.Println("Total: ", myTotal)

	var dog Animal
	dog.Name = "dog"
	dog.Sound = "Bark"
	dog.NumberOfLegs = 4
	dog.Says()

	cat := Animal{
		Name:         "cat",
		Sound:        "Meou",
		NumberOfLegs: 4,
	}

	cat.Says()

}

func sumMany(nums ...int) int {
	total := 0
	for _, x := range nums {
		total = total + x
	}
	return total
}
