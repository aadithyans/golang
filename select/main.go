package main

import (
	"fmt"
	"time"
)

var chan1 = make(chan string)
var chan2 = make(chan string)

func main() {
	var myString = "Goodbye, cruel world!"
	fmt.Println(len(myString))
	fmt.Println(myString[3:5])
}

func task1() {
	time.Sleep(1 * time.Second)
	chan1 <- "one"
}

func task2() {
	time.Sleep(2 * time.Second)
	chan2 <- "two"
}
