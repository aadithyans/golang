package main

import (
	"fmt"
	"log"
	"strconv"

	"github.com/eiannone/keyboard"
)

func main() {
	/*reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("-> ")
		// '_' means, i dont care what the second thing this function returns, just discard
		userInput, _ := reader.ReadString('\n')
		userInput = strings.Replace(userInput, "\n", "", -1) // stripping the white space
		if userInput == "quit" {
			break
		} else {
			fmt.Println(userInput)
		}
	} */
	err := keyboard.Open()

	if err != nil {
		log.Fatal(err)
	}

	// anonymus function.
	// what ever follows the defer keyword, it wont execute immediately.
	// instead it will exec as soon as the current function finished
	defer func() {
		_ = keyboard.Close()
	}()

	coffees := make(map[int]string)
	coffees[1] = "Cappachino"
	coffees[2] = "Latte"
	coffees[3] = "Americano"
	coffees[4] = "Mocha"
	coffees[5] = "Macciato"
	coffees[6] = "Expresso"

	fmt.Println("MENU")
	fmt.Println("----")
	fmt.Println("1. Cappachino")
	fmt.Println("2. Latte")
	fmt.Println("3. Americano")
	fmt.Println("4. Filter")
	fmt.Println("5. Expresso")
	fmt.Println("6. Mocha")
	fmt.Println("Q. Quit")
	for {
		char, _, err := keyboard.GetSingleKey()
		if err != nil {
			log.Fatal(err)
		}

		if char == 'q' || char == 'Q' {
			break
		}

		i, _ := strconv.Atoi(string(char))

		fmt.Println(fmt.Sprintf("You Chose %s", coffees[i]))
	}
	fmt.Println("Program Existing")
}
