package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"threePart/mylogger"
	"time"
)

func main() {
	// three part loop
	for i := 0; i <= 10; i++ {
		fmt.Println("i is ", i)
	}
	rand.Seed(time.Now().UnixNano())
	i := 1000

	// execute a loop while > 1000
	for i > 100 {
		// get a random number
		i = rand.Intn(1000) + 1
		fmt.Println("i is ", i)
		if i > 100 {
			fmt.Println("i is ", i, "so loop keeps going")
		}
	}
	fmt.Println("Got", i, "and broke out of loop")
	infinite()
}

func infinite() {
	// read input from the user 5 timesand write it toa log
	reader := bufio.NewReader(os.Stdin)
	ch := make(chan string)

	go mylogger.ListenForLog(ch)

	fmt.Println("Enter Something")

	for i := 0; i < 5; i++ {
		fmt.Println("->")
		input, _ := reader.ReadString('\n')
		ch <- input
		time.Sleep(time.Second)
	}
}
