package packageone

import "fmt"

var PackageVar = "First Package"

func PrintMe(myVar, blockVar string) {
	fmt.Println(myVar, blockVar, PackageVar)
}
