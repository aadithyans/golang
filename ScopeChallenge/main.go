package main

import (
	"myApp/packageone"
)

var myVar = "This is myVar Variable"

func main() {

	var blockVar = "This is block Var"
	packageone.PrintMe(myVar, blockVar)
}
