package main

import "fmt"

// reference types (pointers, slices, maps, functions, channels)

// pointers is nothing more something points to a specific location in memory
// & -> gets reference of pointer
// * - change value of pointer
func main() {
	x := 10
	y := "Aadithyan"
	myFirstPointer := &x // pointing to the location of the memory where 10 is stored
	mySecondPointer := &y
	fmt.Println("x is ", x)
	fmt.Println("My First Pointer is ", myFirstPointer)

	fmt.Println("y is ", y)
	fmt.Println("My Second Pointer is ", mySecondPointer)

	*myFirstPointer = 15 // * says, go the address in memory where myFirstPointer is pointing and change the contents what ever stored there
	fmt.Println("x is now  ", x)

	*mySecondPointer = "Aadithyan S"
	fmt.Println("y is now  ", y)

	changePointerValue(&x)
	fmt.Println("After x is now  ", x)
}

func changePointerValue(num *int) {
	*num = 25
}
