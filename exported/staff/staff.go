package staff

import "log"

var overPaidLimit = 75000
var underPaidLimit = 30000

type Employee struct {
	FirstName string
	LastName  string
	Salary    int
	FullTime  bool
}

type Office struct {
	AllStaff []Employee
}

func (e *Office) All() []Employee {
	return e.AllStaff
}

func (e *Office) OverPaid() []Employee {
	var overPaid []Employee

	for _, x := range e.AllStaff {
		if x.Salary > overPaidLimit {
			overPaid = append(overPaid, x)
		}
	}
	return overPaid
}

func (e *Office) UnderPaid() []Employee {
	var underPaid []Employee

	for _, x := range e.AllStaff {
		if x.Salary < underPaidLimit {
			underPaid = append(underPaid, x)
		}
	}
	return underPaid
}

func (e *Office) notVisible() {
	log.Println("Hello")

}
