package main

import (
	"exported/staff"
	"log"
)

var employees = []staff.Employee{
	{FirstName: "John", LastName: "Smith", Salary: 30000, FullTime: true},
	{FirstName: "Sam", LastName: "Carren", Salary: 20000, FullTime: false},
	{FirstName: "David", LastName: "Colman", Salary: 40000, FullTime: true},
	{FirstName: "Peter", LastName: "Morris", Salary: 50000, FullTime: true},
	{FirstName: "Maria", LastName: "James", Salary: 150000, FullTime: true},
}

func main() {
	myStaff := staff.Office{
		AllStaff: employees,
	}

	log.Println(myStaff.All())
	log.Println(myStaff.OverPaid())
	log.Println(myStaff.UnderPaid())
}
