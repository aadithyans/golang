package app

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"
)

type CurrentTime struct {
	CurrentTime string `json:"current_time"`
}

func getTime(w http.ResponseWriter, r *http.Request) {
	time_zone := r.URL.Query().Get("tz")
	if time_zone == "" {
		time_zone = "Etc/UTC"
	}

	loc, err := time.LoadLocation(time_zone)
	w.Header().Add("Content-Type", "application/json")

	if err != nil {
		current_time := CurrentTime{
			CurrentTime: "Invalid Time Zone",
		}
		json.NewEncoder(w).Encode(current_time)
	} else {
		fmt.Println(time.Now().In(loc))
		current_time := CurrentTime{
			CurrentTime: time.Now().In(loc).String(),
		}
		json.NewEncoder(w).Encode(current_time)
	}

	r.URL.Query().Get("tz")

}
