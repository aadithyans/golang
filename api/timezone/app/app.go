package app

import (
	"net/http"

	"github.com/gorilla/mux"
)

func Start() {

	router := mux.NewRouter()
	router.HandleFunc("/api/time", getTime).Methods(http.MethodGet)
	http.ListenAndServe("localhost:8000", router)
}
