package app

import (
	"MyApp/service"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

//type Customer struct {
//	Id    string `json:"full_name" xml:"full_name"`
//	City    string `json:"city" xml:"city"`
//	Zipcode string `json:"zip_code" xml:"zip_code"`
//}

func greet(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello Wordld!!!")
}

type CustomerHandlers struct {
	service service.CustomerService
}

func (ch *CustomerHandlers) getAllCustomers(w http.ResponseWriter, r *http.Request) {
	//customers := []Customer{
	//		{Name: "Aadi", City: "Trivandrum", Zipcode: "695031"},
	//		{Name: "Sneha", City: "Trivandrum", Zipcode: "695031"},
	//		{Name: "Deepa", City: "Trivandrum", Zipcode: "695031"},
	//	}
	customers, _ := ch.service.GetAllCustomers()
	content_type := r.Header.Get("Content-Type")
	w.Header().Add("Content-Type", content_type)
	if content_type == "application/json" {
		json.NewEncoder(w).Encode(customers)
	} else {
		xml.NewEncoder(w).Encode(customers)
	}
}

func getCustomer(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	fmt.Fprint(w, vars["customer_id"])
}

func createCustomer(w http.ResponseWriter, r *http.Request) {

	fmt.Fprint(w, "Post Request Received")
}
