package app

import (
	"MyApp/domain"
	"MyApp/service"
	"net/http"

	"github.com/gorilla/mux"
)

func Start() {

	router := mux.NewRouter()

	// Wiring
	handlers := CustomerHandlers{service: service.NewCustomerService(domain.NewCustomerReopsitoryStub())}

	router.HandleFunc("/customers", handlers.getAllCustomers).Methods(http.MethodGet)
	//router.HandleFunc("/greet", greet).Methods(http.MethodGet)

	//router.HandleFunc("/customers", createCustomer).Methods(http.MethodPost)
	//router.HandleFunc("/customers/{customer_id:[0-9]+}", getCustomer).Methods(http.MethodGet)
	http.ListenAndServe("localhost:8000", router)
}
