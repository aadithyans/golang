package domain

type CustomerRepositoryStub struct {
	customers []Customer
}

func (s CustomerRepositoryStub) FindAll() ([]Customer, error) {
	return s.customers, nil
}

func NewCustomerReopsitoryStub() CustomerRepositoryStub {
	customers := []Customer{
		{Id: "1001", Name: "Ashish", City: "New Delhi", Zipcode: "695014", DateofBirth: "28-07-1987", Status: "1"},
		{Id: "1002", Name: "Aadi", City: "Trivandrum", Zipcode: "695014", DateofBirth: "28-07-1984", Status: "1"},
	}
	return CustomerRepositoryStub{customers: customers}
}
