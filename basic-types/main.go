package main

import (
	"fmt"
	"log"
)

// basic types (numbers, strings, booleans)
var myInt int
var myUint uint // should only have positive values

var myFloat float32
var myFload64 float64

// aggregate types (arrays, struct)
type Car struct {
	NumberOfTyres int
	Luxury        bool
	BucketSeats   bool
	Make          string
	Model         string
	Year          int
}

func main() {
	// Basic Types
	myInt = 10
	myUint = 20
	myFloat = 10.1
	myFload64 = 100.10
	log.Println(myInt, myUint, myFloat, myFload64)

	myString := "Aadithyan"

	myString = "S"
	log.Println(myString)

	var myBool = true
	log.Println(myBool)

	// aggregate types
	//arrays
	var myStrings [3]string
	myStrings[0] = "Cat"
	myStrings[1] = "Dog"
	myStrings[2] = "Fish"

	fmt.Println("First Element of Array is: ", myStrings[0])

	var myInts [3]int
	myInts[0] = 1
	myInts[1] = 5
	myInts[2] = 10

	fmt.Println("First Element of array is: ", myInts[0])

	// structs
	myCar := Car{
		NumberOfTyres: 4,
		Luxury:        false,
		Make:          "Maruthi",
		Year:          2017,
		Model:         "Dzire",
		BucketSeats:   true,
	}

	fmt.Printf("My Car is a %d %s %s ", myCar.Year, myCar.Make, myCar.Model)

}
