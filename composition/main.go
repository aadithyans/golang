package main

import "fmt"

type Vehicle struct {
	NumberOfWheels     int
	NumberOfPassengers int
}

type Car struct {
	Make       string
	Model      string
	Year       int
	IsElectric bool
	IsHybrid   bool
	Vehicle    Vehicle
}

func main() {
	suv := Vehicle{
		NumberOfWheels:     4,
		NumberOfPassengers: 6,
	}

	volvo := Car{
		Make:       "Volvo",
		Model:      "Xc90 T10",
		Year:       2021,
		IsElectric: false,
		IsHybrid:   true,
		Vehicle:    suv,
	}
	volvo.show()
	fmt.Println()

	tesla := Car{
		Make:       "VTesla",
		Model:      "Model X",
		Year:       2021,
		IsElectric: true,
		IsHybrid:   false,
		Vehicle:    suv,
	}

	tesla.Vehicle.NumberOfPassengers = 7

	tesla.show()

}

func (v Vehicle) showDetails() {
	fmt.Println("No of Passengers", v.NumberOfPassengers)
	fmt.Println("No of Wheels", v.NumberOfWheels)
}

func (c Car) show() {
	fmt.Println("Make", c.Make)
	fmt.Println("Model", c.Model)
	fmt.Println("Year", c.Year)
	fmt.Println("Electric", c.IsElectric)
	fmt.Println("Hybrid", c.IsHybrid)
	c.Vehicle.showDetails()
}
