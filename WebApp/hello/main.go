package main

import (
	"encoding/json"
	"log"
	"myapp/rps"
	"net/http"
	"strconv"
	"text/template"
)

func homePage(writer http.ResponseWriter, request *http.Request) {
	// html := "<strong>Hello World</strong>"
	// writer.Header().Set("Content-Type", "text/html")
	// fmt.Fprintf(writer, html)
	renderTemplate(writer, "index.html")
}

func playRound(writer http.ResponseWriter, request *http.Request) {
	playerChoice, _ := strconv.Atoi(request.URL.Query().Get("c"))
	result := rps.PlayRound(playerChoice)
	out, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		log.Println(err)
		return
	}
	writer.Header().Set("Content-Type", "text/html")
	writer.Write(out)
}

func main() {
	http.HandleFunc("/play", playRound)
	http.HandleFunc("/", homePage)
	log.Println("Starting Web Server on port 8080")
	http.ListenAndServe(":8080", nil)
}

func renderTemplate(writer http.ResponseWriter, page string) {
	temp, err := template.ParseFiles(page)
	if err != nil {
		log.Println(err)
		return
	}

	err = temp.Execute(writer, nil)
	if err != nil {
		log.Println(err)
		return
	}
}
