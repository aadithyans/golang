package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"
)

const (
	ROCK    = 0
	PAPER   = 1
	SCISSOR = 2
)

func main() {
	rand.Seed(time.Now().UnixNano())
	playerChoice := ""
	playerValue := -1
	computerValue := rand.Intn(2)
	reader := bufio.NewReader(os.Stdin)

	clearScreen()
	fmt.Print("Please enter rock, papers or scissors -> ")
	playerChoice, _ = reader.ReadString('\n')
	playerChoice = strings.Replace(playerChoice, "\n", "", -1)

	if playerChoice == "rock" {
		playerValue = ROCK
	} else if playerChoice == "paper" {
		playerValue = PAPER
	} else if playerChoice == "scissors" {
		playerValue = SCISSOR
	}

	switch computerValue {
	case ROCK:
		fmt.Println("Computer chose rock")
		break
	case PAPER:
		fmt.Println("Computer chose paper")
		break
	case SCISSOR:
		fmt.Println("Computer chose scissors")
		break
	default:
	}
	fmt.Println("Computer value -> ", computerValue)
	fmt.Println("Player value -> ", playerValue)
	fmt.Println()
	if playerValue == computerValue {
		fmt.Println("Its a draw")
	} else {
		switch playerValue {
		case ROCK:
			if computerValue == PAPER {
				fmt.Println("Computer wins")
			} else {
				fmt.Println("Player wins")
			}
			break
		case PAPER:
			if computerValue == SCISSOR {
				fmt.Println("Computer wins")
			} else {
				fmt.Println("Player wins")
			}
			break
		case SCISSOR:
			if computerValue == ROCK {
				fmt.Println("Computer wins")
			} else {
				fmt.Println("Player wins")
			}
			break
		default:
			fmt.Println("Invalid Choice")
		}
	}

	fmt.Println("Player Value is ", playerValue)
}

func clearScreen() {
	if strings.Contains(runtime.GOOS, "windows") {
		cmd := exec.Command("cmd", "/c", "cls")
		cmd.Stdout = os.Stdout
		cmd.Run()
	} else {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}
