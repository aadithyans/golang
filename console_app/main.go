package main

import (
	"fmt"
	"log"

	"github.com/eiannone/keyboard"
)

func main() {
	/*reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("-> ")
		// '_' means, i dont care what the second thing this function returns, just discard
		userInput, _ := reader.ReadString('\n')
		userInput = strings.Replace(userInput, "\n", "", -1) // stripping the white space
		if userInput == "quit" {
			break
		} else {
			fmt.Println(userInput)
		}
	} */
	err := keyboard.Open()

	if err != nil {
		log.Fatal(err)
	}

	// anonymus function.
	// what ever follows the defer keyword, it wont execute immediately.
	// instead it will exec as soon as the current function finished
	defer func() {
		_ = keyboard.Close()
	}()

	fmt.Println("Press any key on the Keyboad, Press ESC to quit")

	for {
		char, key, err := keyboard.GetSingleKey()
		if err != nil {
			log.Fatal(err)
		}
		if key != 0 {
			fmt.Println("You Pressed: ", char, key)
		} else {
			fmt.Println("You Pressed: ", char)
		}

		if key == keyboard.KeyEsc {
			break
		}
	}
	fmt.Println("Program Existing")
}
