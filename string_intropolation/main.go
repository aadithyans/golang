package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/eiannone/keyboard"
)

var reader *bufio.Reader

type User struct {
	UserName        string
	Age             int
	FavouriteNumber float64
	OwnsADog        bool
}

func main() {
	reader = bufio.NewReader(os.Stdin)
	var user User
	user.UserName = readString("What is your Name")

	user.Age = readInt("How old are you")
	user.FavouriteNumber = readFloat("What is your Favourite Number?")
	user.OwnsADog = readOwnsADog(("Do you own a Dog? (y/n)"))
	//fmt.Println(fmt.Sprintf("Your Name is %s. You are %d old", userName, age))
	fmt.Printf("Your Name is %s. You are %d old, Your favourite number is %.2f., Owns a dog %t",
		user.UserName,
		user.Age,
		user.FavouriteNumber, user.OwnsADog)
}

func prompt() {
	fmt.Println("->")
}

func readString(str string) string {
	for {
		fmt.Println(str)
		prompt()
		userInput, _ := reader.ReadString('\n')
		userInput = strings.Replace(userInput, "\r\n", "", -1)
		userInput = strings.Replace(userInput, "\n", "", -1)
		if userInput == "" {
			fmt.Println("Enter a valid Name")
		} else {
			return userInput
		}
	}
}

func readInt(str string) int {
	for {
		fmt.Println(str)
		prompt()
		userInput, _ := reader.ReadString('\n')
		userInput = strings.Replace(userInput, "\r\n", "", -1)
		userInput = strings.Replace(userInput, "\n", "", -1)
		num, err := strconv.Atoi(userInput)
		if err != nil {
			fmt.Println("Please enter a whole number")
		} else {
			return num
		}
	}
}

func readFloat(str string) float64 {
	for {
		fmt.Println(str)
		prompt()
		userInput, _ := reader.ReadString('\n')
		userInput = strings.Replace(userInput, "\r\n", "", -1)
		userInput = strings.Replace(userInput, "\n", "", -1)
		num, err := strconv.ParseFloat(userInput, 64)
		if err != nil {
			fmt.Println("Please enter a number")
		} else {
			return num
		}
	}
}

func readOwnsADog(str string) bool {
	err := keyboard.Open()
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		_ = keyboard.Close()
	}()
	for {
		fmt.Println(str)
		prompt()
		char, _, err := keyboard.GetSingleKey()
		if err != nil {
			log.Fatal(err)
		}
		if strings.ToLower(string(char)) != "y" && strings.ToLower(string(char)) != "n" {
			fmt.Println("Please Type y or n")
		} else if char == 'n' || char == 'N' {
			return false
		} else if char == 'y' || char == 'Y' {
			return true
		}
	}
}
