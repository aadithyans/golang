package main

import (
	"fmt"
	"sort"
)

// slices
func main() {
	var animals []string
	// one way to add elements to slice
	animals = append(animals, "dog")
	animals = append(animals, "cat")
	animals = append(animals, "fish")
	animals = append(animals, "horse")
	fmt.Println("Animals", animals)

	// another

	for index, animal := range animals {
		fmt.Println(index, animal)
	}

	fmt.Println("First 2 ellemens: ", animals[0:2])

	fmt.Println("Total Elements", len(animals))
	fmt.Println("Sorted", sort.StringsAreSorted(animals))
	sort.Strings(animals)
	fmt.Println("Sorted", sort.StringsAreSorted(animals))
	fmt.Println("Animals", animals)
	animals = deleteFromSlice(animals, 2)
	sort.Strings(animals)
	fmt.Println("Animals", animals)
}

func deleteFromSlice(a []string, index int) []string {
	a[index] = a[len(a)-1]
	a[len(a)-1] = ""
	a = a[:len(a)-1]
	return a
}
